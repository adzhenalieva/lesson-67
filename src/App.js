import React, { Component } from 'react';
import EntryPage from "./containers/EntryPage/EntryPage";

class App extends Component {
  render() {
    return (
       <EntryPage/>
    );
  }
}

export default App;
