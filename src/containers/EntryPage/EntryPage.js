import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import './EntryPage.css';

class EntryPage extends Component {
    render() {
        return (
            <Fragment>
                <div className="Wrap">
                    <p>PIN</p>
                    <div className="EntryPage">
                        <div className={this.props.class}>{this.props.codeHidden}</div>
                        <button onClick={() => this.props.addNumber(7)}>7</button>
                        <button onClick={() => this.props.addNumber(8)}>8</button>
                        <button onClick={() => this.props.addNumber(9)}>9</button>
                        <button onClick={() => this.props.addNumber(4)}>4</button>
                        <button onClick={() => this.props.addNumber(5)}>5</button>
                        <button onClick={() => this.props.addNumber(6)}>6</button>
                        <button onClick={() => this.props.addNumber(1)}>1</button>
                        <button onClick={() => this.props.addNumber(2)}>2</button>
                        <button onClick={() => this.props.addNumber(3)}>3</button>
                        <button onClick={this.props.deleteNumber}>&lt;</button>
                        <button onClick={() => this.props.addNumber(0)}>0</button>
                        <button onClick={this.props.enterPin}>E</button>
                    </div>
                </div>
                <div className="Wrap">
                    <p>Calc</p>
                    <div className="EntryPage">
                        <div className="Check">{this.props.math}</div>
                        <button onClick={() => this.props.addCalc(7)}>7</button>
                        <button onClick={() => this.props.addCalc(8)}>8</button>
                        <button onClick={() => this.props.addCalc(9)}>9</button>
                        <button onClick={() => this.props.addCalc(4)}>4</button>
                        <button onClick={() => this.props.addCalc(5)}>5</button>
                        <button onClick={() => this.props.addCalc(6)}>6</button>
                        <button onClick={() => this.props.addCalc(1)}>1</button>
                        <button onClick={() => this.props.addCalc(2)}>2</button>
                        <button onClick={() => this.props.addCalc(3)}>3</button>
                        <button onClick={() => this.props.addCalc(0)}>0</button>
                        <button onClick={() => this.props.addCalc('+')}>+</button>
                        <button onClick={() => this.props.addCalc('-')}>-</button>
                        <button onClick={() => this.props.addCalc('*')}>*</button>
                        <button onClick={() => this.props.addCalc('/')}>/</button>
                        <button onClick={this.props.removeCalc}>&lt;</button>
                        <button onClick={this.props.makeCalc}>=</button>
                    </div>
                </div>
            </Fragment>


        );
    }
}

const mapStateToProps = state => {
    return {
        code: state.code,
        codeHidden: state.codeHidden,
        class: state.class,
        math: state.math
    };
};

const mapDispatchToProps = dispatch => {
    return {
        addNumber: number => dispatch({type: 'ADD_NUMBER', number}),
        deleteNumber: () => dispatch({type: 'DELETE_NUMBER'}),
        enterPin: () => dispatch({type: 'ENTER_PIN'}),
        addCalc: number => dispatch({type: 'ADD_CALC', number}),
        makeCalc: () => dispatch({type: 'MAKE_CALC'}),
        removeCalc: () => dispatch({type: 'REMOVE_CALC'})
    };

};


export default connect(mapStateToProps, mapDispatchToProps)(EntryPage);