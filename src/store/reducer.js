const initialState = {

    code: "",
    class: "Check",
    codeHidden: '',
    pin: 1234,
    math: ''

};

const reducer = (state = initialState, action) => {
    switch (action.type) {

        case 'ADD_NUMBER':
            let codeHidden;
            if (state.codeHidden.length < 8) {
                codeHidden = state.codeHidden + "* ";
            } else {
                alert('You may not enter more than 4 numbers');
                codeHidden = state.codeHidden;
            }

            return {
                ...state, code: state.code + action.number, codeHidden: codeHidden
            };
        case 'DELETE_NUMBER':
            const newCode = state.code.substr(0, state.code.length - 1);
            const newCodeHidden = state.codeHidden.substr(0, state.codeHidden.length - 1);
            return {
                ...state, code: newCode, codeHidden: newCodeHidden
            };
        case 'ENTER_PIN':

            if (parseInt(state.code) === state.pin) {
                return {
                    ...state, codeHidden: 'Access Granted', class: "Access"
                };
            }
            alert('Incorrect pin. Try again');
            return {
                ...state, codeHidden: '', code: ""
            };
        case 'ADD_CALC':
            return {
                ...state, math: state.math + action.number
            };
        case 'MAKE_CALC':
            return {
                ...state, math: String(eval(state.math))
            };
        case 'REMOVE_CALC':
            const newMATH = state.math.substr(0, state.math.length - 1);
            return {
                ...state, math: newMATH
            };
        default:
            return state;
    }


};


export default reducer;